#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <time.h>
#include <sys/wait.h>

void remove_directory(const char *directory);
void run_command(const char *command, char *const argv[]);
size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream);
void download_and_unzip_file(const char *url);
void select_random_image(const char *directory);
void move_images_to_categories(const char *source_directory);
void zip_directories(const char *directory);

int main() {
    const char *url = "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq";
    const char *source_directory = "kebun_binatang";

    download_and_unzip_file(url);
    select_random_image(source_directory);
    move_images_to_categories(source_directory);
    zip_directories("HewanDarat");
    remove_directory("HewanDarat");
    zip_directories("HewanAmphibi");
    remove_directory("HewanAmphibi");
    zip_directories("HewanAir");
    remove_directory("HewanAir");
    return 0;
}

void run_command(const char *command, char *const argv[]) {
    pid_t pid;
    int status;

    pid = fork();
    if (pid == 0) {
        // Child process
        if (execvp(command, argv) == -1) {
            perror("execvp");
        }
        exit(EXIT_FAILURE);
    } else if (pid < 0) {
        // Fork error
        perror("fork");
    } else {
        // Parent process
        do {
            waitpid(pid, &status, WUNTRACED);
        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }
}

void download_and_unzip_file(const char *url) {
    char *wget_argv[] = {"wget", "--quiet", "--no-check-certificate", "--load-cookies", "/tmp/cookies.txt", "--save-cookies", "/tmp/cookies.txt", "--keep-session-cookies", "--output-document", "-", (char *)url, NULL};
    run_command("wget", wget_argv);

    char *wget_confirm_argv[] = {"wget", "--quiet", "--no-check-certificate", "--load-cookies", "/tmp/cookies.txt", "--output-document", "kebun_binatang.zip", (char *)url, NULL};
    run_command("wget", wget_confirm_argv);

    char *unzip_argv[] = {"unzip", "kebun_binatang.zip", "-d", "kebun_binatang", NULL};
    run_command("unzip", unzip_argv);
}

void select_random_image(const char *directory) {
    DIR *dir;
    struct dirent *entry;
    int count = 0;

    dir = opendir(directory);
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            count++;
        }
    }
    closedir(dir);

    int random_number = rand() % count;
    count = 0;
    dir = opendir(directory);
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            if (count == random_number) {
                printf("Grape-kun akan melakukan shift penjagaan pada hewan: %s\n", entry->d_name);
                break;
            }
            count++;
        }
    }
    closedir(dir);
}

void move_images_to_categories(const char *source_directory) {
    DIR *dir;
    struct dirent *entry;
    const char *directories[] = {"HewanDarat", "HewanAmphibi", "HewanAir"};
    int i;

    for (i = 0; i < 3; i++) {
        mkdir(directories[i], 0700);
    }

    dir = opendir(source_directory);
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            char src[256], dest[256];

            snprintf(src, sizeof(src), "%s/%s", source_directory, entry->d_name);

            if (strstr(entry->d_name, "darat") != NULL) {
                snprintf(dest, sizeof(dest), "HewanDarat/%s", entry->d_name);
            } else if (strstr(entry->d_name, "amphibi") != NULL) {
                snprintf(dest, sizeof(dest), "HewanAmphibi/%s", entry->d_name);
            } else if (strstr(entry->d_name, "air") != NULL) {
                snprintf(dest, sizeof(dest), "HewanAir/%s", entry->d_name);
            } else {
                continue;
            }

            rename(src, dest);
        }
    }
    closedir(dir);
}

void zip_directories(const char *directory) {
    char zip_filename[256];
    snprintf(zip_filename, sizeof(zip_filename), "%s.zip", directory);

    char *zip_argv[] = {"zip", "-r", zip_filename, (char *)directory, NULL};
    run_command("zip", zip_argv);
}

void remove_directory(const char *directory) {
    char *rm_argv[] = {"rm", "-r", (char *)directory, NULL};
    run_command("rm", rm_argv);
}
