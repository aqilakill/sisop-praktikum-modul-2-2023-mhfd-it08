//selesai poin A,B,C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>


void move_files(char *folder_name, char **file_list, int file_count) {
	char folder_path[20];
	sprintf(folder_path, "players/%s", folder_name);
	for (int i = 0; i < file_count; i++) {
    	char old_path[50], new_path[50];
    	sprintf(old_path, "players/%s", file_list[i]);
    	sprintf(new_path, "%s/%s", folder_path, file_list[i]);
    	int pid = fork();
    	if (pid == -1) {
        	printf("Failed to create child process\n");
        	exit(1);
    	} else if (pid == 0) {
        	execl("/bin/mv", "mv", old_path, new_path, NULL);
        	exit(0);
    	} else {
        	wait(NULL);
    	}
	}
}

void unzip_and_delete(char* filename) {
    int pid = fork();
    if (pid == -1) {
   	 printf("Failed to create child process\n");
   	 exit(1);
    }
    else if (pid == 0) {
   	 execl("/usr/bin/unzip", "unzip", filename, NULL);
   	 exit(0);
    }
    else {
   	 wait(NULL);
   	 printf("Unzipped file successfully.\n");
   	 remove(filename);
   	 printf("Deleted zip file.\n");
    }
}

void remove_non_manutd_files() {
    DIR *dir = opendir("players");
    if (dir == NULL) {
   	 printf("Failed to open directory\n");
   	 exit(1);
    }
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
   	 if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") ==  0) {
   		 continue;
   	 }
   	 char *file_path = malloc(strlen(entry->d_name) + 12); // Add 12 for "players/" and null terminator
   	 sprintf(file_path, "players/%s", entry->d_name);
   	 if (strstr(entry->d_name, "ManUtd") == NULL) {
   		 remove(file_path);
   		 printf("Deleted file: %s\n", file_path);
   	 }
   	 free(file_path);
    }
    closedir(dir);
}

void create_folders() {
    int pid;
    char *folders[] = {"Kiper", "Gelandang", "Bek", "Penyerang"};

    for (int i = 0; i < 4; i++) {
   	 pid = fork();
   	 if (pid == -1) {
   		 printf("Failed to create child process\n");
   		 exit(1);
   	 } else if (pid == 0) {
   		 char folder_path[20];
   		 sprintf(folder_path, "players/%s", folders[i]);
   		 execl("/bin/mkdir", "mkdir", folder_path, NULL);
   		 exit(0);
   	 } else {
   		 wait(NULL);
   		 printf("Created folder: %s\n", folders[i]);
   	 }
    }
}


void sort_files_to_folders() {
	DIR *dir = opendir("players");
	if (dir == NULL) {
    	printf("Failed to open directory\n");
    	exit(1);
	}
	struct dirent *entry;
	char *folders[] = {"Kiper", "Gelandang", "Bek", "Penyerang"};
	char *file_lists[4][100];
	int file_counts[4] = {0};

	while ((entry = readdir(dir)) != NULL) {
    	if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") ==  0) {
        	continue;
    	}
    	char *file_name = entry->d_name;
    	if (strstr(file_name, "Kiper") != NULL) {
        	file_lists[0][file_counts[0]] = file_name;
        	file_counts[0]++;
    	} else if (strstr(file_name, "Gelandang") != NULL) {
        	file_lists[1][file_counts[1]] = file_name;
        	file_counts[1]++;
    	} else if (strstr(file_name, "Bek") != NULL) {
        	file_lists[2][file_counts[2]] = file_name;
        	file_counts[2]++;
    	} else if (strstr(file_name, "Penyerang") != NULL) {
        	file_lists[3][file_counts[3]] = file_name;
        	file_counts[3]++;
    	}
	}

	for (int i = 0; i < 4; i++) {
    	move_files(folders[i], file_lists[i], file_counts[i]);
    	printf("Moved %d files to folder %s\n", file_counts[i], folders[i]);
	}

	closedir(dir);
}


int main()
{
int pid = fork();
if (pid == -1) {
printf("Failed to create child process\n");
exit(1);
}
else if (pid == 0) {
execl("/usr/bin/wget", "wget", "--no-check-certificate", "--output-document=players.zip", "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", NULL);
exit(0);
}
else {
wait(NULL);
printf("Downloaded file successfully.\n");
unzip_and_delete("players.zip");
remove_non_manutd_files();
create_folders();
sort_files_to_folders();
}
return 0;
}
