#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>

int main(int argc, char *argv[]) {
    pid_t pid, sid;        // Variabel untuk menyimpan PID

    pid = fork();     // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
    * (nilai variabel pid < 0) */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/home/Izen/Sisop/Modul2/soal4")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    char *command[1000];
    int hour, minute, second;
    struct tm *tm_ptr;
    time_t now;
    pid_t child_pid;

    // Memeriksa argumen dan menyimpan path file script bash ke dalam array command
    if (argc != 6) {
        syslog(LOG_ERR, "Invalid arguments");
        exit(EXIT_FAILURE);
    } else {
        if (strcmp(argv[1], "*") == 0) {
            hour = -1;
        } else {
            hour = atoi(argv[1]);
    if (hour < 0 || hour > 23) {
            syslog(LOG_ERR, "Invalid hour argument");
            exit(EXIT_FAILURE);
        }
    }
    if (strcmp(argv[2], "*") == 0) {
        minute = -1;
    } else {
        minute = atoi(argv[2]);
        if (minute < 0 || minute > 59) {
            syslog(LOG_ERR, "Invalid minute argument");
            exit(EXIT_FAILURE);
        }
    }
    if (strcmp(argv[3], "*") == 0) {
        second = -1;
    } else {
        second = atoi(argv[3]);
        if (second < 0 || second > 59) {
            syslog(LOG_ERR, "Invalid second argument");
            exit(EXIT_FAILURE);
        }
    }
    command[0] = argv[5];
    for (int i = 1; i < argc - 1; i++) {
        command[i] = argv[i + 1];
    }
    command[argc - 1] = NULL;
}

// Menjalankan program pada daemon
while (1) {
    time(&now);
    tm_ptr = localtime(&now);

    if ((hour == -1 || hour == tm_ptr->tm_hour) &&
        (minute == -1 || minute == tm_ptr->tm_min) &&
        (second == -1 || second == tm_ptr->tm_sec)) {
        child_pid = fork();
        if (child_pid == 0) {
            execv(command[0], command);
            exit(EXIT_SUCCESS);
        } else if (child_pid == -1) {
            syslog(LOG_ERR, "Failed to fork child process");
            exit(EXIT_FAILURE);
        }
    }
    sleep(1);
}
exit(EXIT_SUCCESS);
}
